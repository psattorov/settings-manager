<?php
require_once 'app/Repositories/UserRepository.php';
require_once 'app/Repositories/SettingRepository.php';
require_once 'app/Repositories/ConfirmationCodeRepository.php';
require_once 'app/Interfaces/UserInterface.php';
require_once 'app/Interfaces/SettingInterface.php';
require_once 'app/Interfaces/ConfirmationCodeInterface.php';

$userInterface = new UserRepository();
$settingInterface = new SettingRepository();
$confirmationCodeInterface = new ConfirmationCodeRepository();

// Usage example for updating a setting and generating a confirmation code

$userId = 1;
$settingId = 1;
$newValue = "bell";

// Fetch the user and setting information
$user = $userInterface->getUserById($userId);
$setting = $settingInterface->getSettingById($settingId);

// Generate a confirmation code
$confirmationCode = $confirmationCodeInterface->generateCode($user->id, $setting->id);

// Send the confirmation code to the user using the selected method (SMS/Email/Telegram)
$confirmationMethod = "SMS";
// ... Logic to send the code using the selected method


// Verify the confirmation code
$isValid = $confirmationCodeInterface->verifyCode($user->id, $confirmationCode);

if ($isValid) {
	$setting->value = $newValue;
    $settingInterface->updateSetting($setting);
} else {
    echo "Invalid confirmation code.";
}
