<?php
namespace app\Models;

class Setting {
    public $id;
    public $userId;
    public $name;
    public $value;
}