<?php
namespace app\Interfaces;

use app\Models\Setting;

interface SettingInterface {
    public function getSettingById($settingId): Setting;
    public function updateSetting($setting);
}
