<?php
namespace app\Interfaces;

use app\Models\User;

interface UserInterface {
    public function getUserById($userId): User;
    public function getUserByEmail($email): User;
}