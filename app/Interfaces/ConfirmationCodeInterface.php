<?php
namespace app\Interfaces;

interface ConfirmationCodeInterface {
    public function generateCode($userId, $settingId);
    public function verifyCode($userId, $code);
}
