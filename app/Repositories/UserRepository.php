<?php
require_once 'app/Models/User.php';
require_once 'app/Interfaces/UserInterface.php';

use app\Models\User;
use app\Interfaces\UserInterface;

// Dummy implementation for UserInterface
class UserRepository implements UserInterface {
    public function getUserById($userId): User {
        // Logic to fetch user by ID from the database
        $user = new User();
        $user->id = $userId;
        $user->name = "John Doe";
        $user->email = "john@example.com";
        $user->phoneNumber = "1234567890";

        return $user;
    }

    public function getUserByEmail($email): User {
        // Logic to fetch user by email from the database
        $user = new User();
        $user->id = 1;
        $user->name = "John Doe";
        $user->email = $email;
        $user->phoneNumber = "1234567890";

        return $user;
    }
}