<?php
require_once 'app/Models/Setting.php';
require_once 'app/Interfaces/SettingInterface.php';

use app\Models\Setting;
use app\Interfaces\SettingInterface;

// Dummy implementation for SettingInterface
class SettingRepository implements SettingInterface {
    public function getSettingById($settingId): Setting {
        // Logic to fetch setting by ID from the database
        $setting = new Setting();
        $setting->id = $settingId;
        $setting->userId = 1;
        $setting->name = "NotificationSound";
        $setting->value = "chime";

        return $setting;
    }

    public function updateSetting($setting) {
        // Logic to update setting in the database
        echo "Setting updated: " . $setting->name . " = " . $setting->value;
    }
}